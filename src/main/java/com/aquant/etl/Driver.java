package com.aquant.etl;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Driver {

    static Connection connection= null;
    static Statement statement = null;

    public static void main(String[] args) throws Exception {
        HashMap<String,String> config = getConfig(args[0]);
        final String db_url 	= config.get("db_url");
        final String db_uname	= config.get("db_uname");
        final String db_pword	= config.get("db_pword");
        final String ftbl_name 	= config.get("update_finance_tbl_name");
        final String stbl_name 	= config.get("update_sentiment_tbl_name");
        final String ttbl_name 	= config.get("ticker_tbl_name");


        ArrayList<String> codeList = new ArrayList<String>();
        Class.forName("org.postgresql.Driver");
        connection = DriverManager
                .getConnection(db_url,db_uname, db_pword);
        statement = connection.createStatement();
        ResultSet rst= statement.executeQuery( "select code from " + ttbl_name);

        while (rst.next())
            codeList.add(rst.getString(1));

//    	System.out.println(codeList.size());
//    	System.out.println(codeList);
//    	statement.execute("truncate " + code_list);
//    	connection.setAutoCommit(false);
        ExecutorService pool = Executors.newFixedThreadPool(1);

        for ( int i = 0; i < codeList.size(); i ++){
            Thread thread = new CleanLoad(
                    codeList.get(i)+".AX",
                    connection,
                    ftbl_name,
                    stbl_name,
                    ttbl_name);

            pool.execute(thread);

        }


        pool.shutdown();
    }

    private static HashMap<String, String> getConfig(String input) throws IOException {
        String line = null;
        FileReader fileReader =  new FileReader(input);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        HashMap<String, String> ans = new HashMap<String,String>();
        while((line = bufferedReader.readLine()) != null) {
            ans.put(line.substring(0, line.indexOf("=")), line.substring(line.indexOf("=")+1,line.length()));
        }
        bufferedReader.close();
        fileReader.close();
        return ans;
    }
}

