package com.aquant.etl;

import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.Statement;

import org.apache.commons.io.IOUtils;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

public class CleanLoad  extends Thread {


    private String code;
    private Connection connection;
    private String ftbl_name;
    private String stbl_name;
    private String ttbl_name;

    private static Statement statement;
    private static String	currentPrice = "";
    private static String	targetLowPrice = "";
    private static String	targetMedianPrice = "";
    private static String	numberOfAnalystOpinions = "";
    private static String	targetMeanPrice = "";
    private static String	targetHighPrice = "";
    private static String	recommendationMean = "";
    private static String	regularMarketOpen = "";
    private static String	regularMarketDayHigh = "";
    private static String	regularMarketPreviousClose = "";
    private static String	regularMarketDayLow = "";
    private static String	trailingPE = "";
    private static String	regularMarketVolume = "";
    private static String	averageVolume = "";
    private static String	ask = "";
    private static String	askSize = "";
    private static String	forwardPE = "";
    private static String	bid = "";
    private static String	bidSize = "";
    private static String	MarketOpen = "";
    private static String	MarketDayHigh = "";
    private static String	MarketPreviousClose = "";
    private static String	MarketDayLow = "";
    private static String	MarketPrice = "";
    private static String	MarketVolume = "";
    private static String	enRevenue  = "";
    private static String	profitMargins  = "";
    private static String	enEbitda  = "";
    private static String	bookValue  = "";
    private static String	trailingEps  = "";
    private static String	priceToBook  = "";
    private static String	beta  = "";
    private static String	eGrowth  = "";
    private static String	pubTitle = "";
    private static String	pubTime  = "";


    public CleanLoad(String code, Connection connection, String ftbl_name, String stbl_name, String ttbl_name) {
        super();
        this.code = code;
        this.connection = connection;
        this.ftbl_name = ftbl_name;
        this.stbl_name = stbl_name;
        this.ttbl_name = ttbl_name;

    }


    public void run() {

        String extraction = null;
        try {
            statement = connection.createStatement();
            URL url = new URL("https://au.finance.yahoo.com/quote/"+code);
            String result = IOUtils.toString(url.openStream(), StandardCharsets.UTF_8);
            String subresult= result.substring(result.indexOf("root.App.main ="));
            extraction= subresult.substring(16,subresult.indexOf("\n")-1);


            JsonParser jParser = new JsonParser();
            JsonElement org_jElement = jParser.parse(extraction);
            JsonElement q_jElement = null;
            JsonElement s_jElement = null;

            boolean quote_exists = checkQuoteSummary(org_jElement);
            boolean steam_exists = checkSteam(org_jElement,code);
//		System.out.println(quote_exists);
//		System.out.println(steam_exists);


            if(quote_exists)	q_jElement = cleanQuoteElement(org_jElement);
            if(steam_exists)   	s_jElement = cleanSteamElement(org_jElement, code);

            if(steam_exists){
                for ( int i = 0 ; i < s_jElement.getAsJsonArray().size(); i ++){
                    JsonElement temp_jElement = s_jElement.getAsJsonArray().get(i).getAsJsonObject();
                    if(temp_jElement.getAsJsonObject().has("title"))
                        pubTitle =temp_jElement.getAsJsonObject().get("title").getAsString();
                    if(temp_jElement.getAsJsonObject().has("pubtime"))
                        pubTime =temp_jElement.getAsJsonObject().get("pubtime").getAsString();
                    String sql_sentiment = "insert into "+stbl_name+" select '"+ code + "','"
                            + pubTitle.replaceAll("'", "''")          + "','"
                            + pubTime       + "';";
                    statement.execute(sql_sentiment);
                }
            }

            if(quote_exists&&checkNextElement(q_jElement, "defaultKeyStatistics")){
                enRevenue = getElement(q_jElement, "defaultKeyStatistics", "enterpriseToRevenue");
                profitMargins = getElement(q_jElement, "defaultKeyStatistics", "profitMargins");
                enEbitda = getElement(q_jElement, "defaultKeyStatistics", "enterpriseToEbitda");
                bookValue = getElement(q_jElement, "defaultKeyStatistics", "bookValue");
                trailingEps = getElement(q_jElement, "defaultKeyStatistics", "trailingEps");
                priceToBook = getElement(q_jElement, "defaultKeyStatistics", "priceToBook");
                beta = getElement(q_jElement, "defaultKeyStatistics", "beta");
                eGrowth = getElement(q_jElement, "defaultKeyStatistics", "earningsQuarterlyGrowth");
            }

            if(quote_exists&&checkNextElement(q_jElement, "price")){
                MarketOpen = getElement(q_jElement, "price", "regularMarketOpen");
                MarketDayHigh = getElement(q_jElement, "price", "regularMarketDayHigh");
                MarketPreviousClose = getElement(q_jElement, "price", "regularMarketPreviousClose");
                MarketDayLow = getElement(q_jElement, "price", "regularMarketDayLow");
                MarketPrice = getElement(q_jElement, "price", "regularMarketPrice");
                MarketVolume = getElement(q_jElement, "price", "regularMarketVolume");
            }

            if(quote_exists&&checkNextElement(q_jElement, "financialData")){
                currentPrice =  getElement(q_jElement, "financialData", "currentPrice");
                targetLowPrice = getElement(q_jElement, "financialData", "targetLowPrice");
                targetMedianPrice = getElement(q_jElement, "financialData", "targetMedianPrice");
                numberOfAnalystOpinions = getElement(q_jElement, "financialData", "numberOfAnalystOpinions");
                targetMeanPrice = getElement(q_jElement, "financialData", "targetMeanPrice");
                targetHighPrice = getElement(q_jElement, "financialData", "targetHighPrice");
                recommendationMean = getElement(q_jElement, "financialData", "recommendationMean");
            }

            if(quote_exists&&checkNextElement(q_jElement, "summaryDetail")){
                regularMarketOpen = getElement(q_jElement, "summaryDetail", "regularMarketOpen");
                regularMarketDayHigh = getElement(q_jElement, "summaryDetail", "regularMarketDayHigh");
                regularMarketPreviousClose = getElement(q_jElement, "summaryDetail", "regularMarketPreviousClose");
                regularMarketDayLow = getElement(q_jElement, "summaryDetail", "regularMarketDayLow");
                trailingPE = getElement(q_jElement, "summaryDetail", "trailingPE");
                regularMarketVolume = getElement(q_jElement, "summaryDetail", "regularMarketVolume");
                averageVolume = getElement(q_jElement, "summaryDetail", "averageVolume");
                ask = getElement(q_jElement, "summaryDetail", "ask");
                askSize = getElement(q_jElement, "summaryDetail", "askSize");
                forwardPE = getElement(q_jElement, "summaryDetail", "forwardPE");
                bid = getElement(q_jElement, "summaryDetail", "bid");
                bidSize = getElement(q_jElement, "summaryDetail", "bidSize");
            }

            String sql_finance = "insert into "+ftbl_name+" select '"+ code + "','"
                    + MarketOpen + "','"
                    + MarketDayHigh + "','"
                    + MarketPreviousClose + "','"
                    + MarketDayLow + "','"
                    + MarketPrice + "','"
                    + MarketVolume + "','"
                    + currentPrice + "','"
                    + targetLowPrice + "','"
                    + targetMedianPrice + "','"
                    + numberOfAnalystOpinions + "','"
                    + targetMeanPrice + "','"
                    + targetHighPrice + "','"
                    + recommendationMean + "','"
                    + regularMarketOpen + "','"
                    + regularMarketDayHigh + "','"
                    + regularMarketPreviousClose + "','"
                    + regularMarketDayLow + "','"
                    + trailingPE + "','"
                    + regularMarketVolume + "','"
                    + averageVolume + "','"
                    + ask + "','"
                    + askSize + "','"
                    + forwardPE + "','"
                    + bid + "','"
                    + bidSize + "','"
                    + enRevenue     + "','"
                    + profitMargins + "','"
                    + enEbitda      + "','"
                    + bookValue     + "','"
                    + trailingEps   + "','"
                    + priceToBook   + "','"
                    + beta          + "','"
                    + eGrowth       + "';";



            statement.execute(sql_finance);
//			System.out.println("delete from "+ttbl_name + " where code = '" + code.substring(0,code.length()-3) + "';");
            statement.execute("delete from "+ttbl_name + " where code = '" + code.substring(0,code.length()-3) + "';");

        } catch (Exception e) {
//			Thread.currentThread().interrupt();
            return;
        }

    }
    private static boolean checkNextElement(JsonElement jElement,String tag) {
        return jElement.getAsJsonObject().has(tag);
    }

    private static boolean checkSteam(JsonElement jElement, String code) {
        return (jElement.getAsJsonObject().has("context"))
                &&(jElement
                .getAsJsonObject().get("context")
                .getAsJsonObject().has("dispatcher"))
                &&(jElement
                .getAsJsonObject().get("context")
                .getAsJsonObject().get("dispatcher")
                .getAsJsonObject().has("stores"))
                &&(jElement
                .getAsJsonObject().get("context")
                .getAsJsonObject().get("dispatcher")
                .getAsJsonObject().get("stores")
                .getAsJsonObject().has("StreamStore"))
                &&(jElement
                .getAsJsonObject().get("context")
                .getAsJsonObject().get("dispatcher")
                .getAsJsonObject().get("stores")
                .getAsJsonObject().get("StreamStore")
                .getAsJsonObject().has("streams"))
                &&(jElement
                .getAsJsonObject().get("context")
                .getAsJsonObject().get("dispatcher")
                .getAsJsonObject().get("stores")
                .getAsJsonObject().get("StreamStore")
                .getAsJsonObject().get("streams")
                .getAsJsonObject().has("YFINANCE:"+code+".mega"))
                &&(jElement
                .getAsJsonObject().get("context")
                .getAsJsonObject().get("dispatcher")
                .getAsJsonObject().get("stores")
                .getAsJsonObject().get("StreamStore")
                .getAsJsonObject().get("streams")
                .getAsJsonObject().get("YFINANCE:"+code+".mega")
                .getAsJsonObject().has("data"))
                &&(jElement
                .getAsJsonObject().get("context")
                .getAsJsonObject().get("dispatcher")
                .getAsJsonObject().get("stores")
                .getAsJsonObject().get("StreamStore")
                .getAsJsonObject().get("streams")
                .getAsJsonObject().get("YFINANCE:"+code+".mega")
                .getAsJsonObject().get("data")
                .getAsJsonObject().has("stream_items"))
                &&(jElement
                .getAsJsonObject().get("context")
                .getAsJsonObject().get("dispatcher")
                .getAsJsonObject().get("stores")
                .getAsJsonObject().get("StreamStore")
                .getAsJsonObject().get("streams")
                .getAsJsonObject().get("YFINANCE:"+code+".mega")
                .getAsJsonObject().get("data")
                .getAsJsonObject().get("stream_items")
                .isJsonArray())
                ;
    }

    private static boolean checkQuoteSummary(JsonElement jElement) {
        return (jElement.getAsJsonObject().has("context"))
                &&(jElement
                .getAsJsonObject().get("context")
                .getAsJsonObject().has("dispatcher"))
                &&(jElement
                .getAsJsonObject().get("context")
                .getAsJsonObject().get("dispatcher")
                .getAsJsonObject().has("stores"))
                &&(jElement
                .getAsJsonObject().get("context")
                .getAsJsonObject().get("dispatcher")
                .getAsJsonObject().get("stores")
                .getAsJsonObject().has("QuoteSummaryStore"));
    }

    private static JsonElement cleanSteamElement(JsonElement jElement, String code) {
        return  jElement.getAsJsonObject().get("context")
                .getAsJsonObject().get("dispatcher")
                .getAsJsonObject().get("stores")
                .getAsJsonObject().get("StreamStore")
                .getAsJsonObject().get("streams")
                .getAsJsonObject().get("YFINANCE:"+code+".mega")
                .getAsJsonObject().get("data")
                .getAsJsonObject().get("stream_items")
                .getAsJsonArray();
    }

    private static JsonElement cleanQuoteElement(JsonElement jElement) {
        return  jElement.getAsJsonObject().get("context")
                .getAsJsonObject().get("dispatcher")
                .getAsJsonObject().get("stores")
                .getAsJsonObject().get("QuoteSummaryStore");
    }

    private static String getElement(JsonElement jElement, String category, String tag) {
        if	((jElement.getAsJsonObject().has(category))
                && (jElement.getAsJsonObject().get(category)
                .getAsJsonObject().has(tag))
                && (jElement.getAsJsonObject().get(category)
                .getAsJsonObject().get(tag)
                .getAsJsonObject().has("raw"))){
            return jElement.getAsJsonObject().get(category)
                    .getAsJsonObject().get(tag)
                    .getAsJsonObject().get("raw").getAsString();
        }else	return "";
    }

}



